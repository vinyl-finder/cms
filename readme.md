# cms for vinyl-finder and google cse annotations.xml
Using decap-cms to make an interface to manage in CI/CD the
`annotations.xml` file of google Custom Search Engines.

> Trying to be general, but use cases are for vinyl-search.gitlab.io

It manages two content collections:
- `sites` converted from/to annotations
- `labels` associated to `site.labels` in the annotations

# Development
Run `npm install` to install the development dependencies.

# Usage
It exposes `npm run` scripts to convert `in/annotations.xml` into the
CMS collections files:
- `annotations-to-sites` for `content/sites/*.json`
- `annotations-to-labels` for `content/labels/*.json`

It also expose a script `sites-to-annotations` to convert the
`sites/*.json` into a `out/annotations.xml` file, which could be
uploaded as the new CSE annotations configuration.

## Local CMS

To run the cms locally:
- `npm run dev` to serve the `index.html` file with the CMS
- `npm run cms` to serve the decap-cms local backend proxy, to CRUD
  local files

## Production
> Not yet

### CMS

Under [gitlab group settings, an application](https://gitlab.com/groups/vinyl-finder/-/settings/applications), redirect URI ([full config](https://decapcms.org/docs/gitlab-backend/) with PKCE backend) and `API` access grant.
```
https://vinyl-finder.gitlab.io/cms
https://vinyl-finder.gitlab.io/cms/
```

See the backend config file for all details.

# Examples
If this repository is cloned, and has an empty `/content/` folder,
replace the `in/annotations.xml` file, with the one downloaded form
the google CSE website, then run the conversion script to generate the
`sites` ad `labels` collections.


# Todo
Considering on the roadmap:
- currently considering all annotations as "included", should handle
  the "excluded" scenario (so it does not index the annotation's
  specified URL patterns)
- CI/CD to publish the resulting XML from `.env` var `CSE_API_KEY`
  (google cloud/adsence), if possible; that way each change to the
  content would upload a fresh XML definition.

# Docs
Good links:
- https://developers.google.com/custom-search/docs/annotations
- https://developers.google.com/custom-search/docs/ranking
- https://cse.google.com

# Limitations
Known limitations to this project and the ecosystem.
- https://developers.google.com/custom-search/docs/annotations

-  5000 annotations max in xml, from google CSE annotations.xml upload UI:

> Note: There are a maximum of 5,000 annotations allowed in a search engine.

- 30 kilobytes file size max

- deleting a `label` from the collection does not updatate the
  `site.labels` (no automatic relationships). First delete the label
  on each sites, then delete the label itself to cleanly delete from
  the CMS

- the diff feature is not yet practical since the sites/annotations
  doe not preserve timestamp, and ordering of the items in the xml
  files; it is hard to have a clear overview of what changed or not
