#!/usr/bin/env node

import fs from "fs/promises";
import path from "path";
import xml2js from "xml2js";

// Function to parse XML data
async function parseXml(xmlData) {
	return await xml2js.parseStringPromise(xmlData);
}

// Function to write JSON data to file
async function writeJsonToFile(filePath, jsonData) {
	await fs.writeFile(filePath, JSON.stringify(jsonData, null, 2));
}

// Main function to import XML to JSON
async function importXmlToJson(xmlFilePath) {
	try {
		// Read XML file
		const xmlData = await fs.readFile(xmlFilePath, "utf-8");

		// Parse XML data
		const jsonData = await parseXml(xmlData);

		// Get output file path
		const outputFilePath = path.join(process.cwd(), "content", "context.json");

		// Write JSON data to file
		await writeJsonToFile(outputFilePath, jsonData);

		console.log("XML imported and converted to JSON successfully.");
	} catch (error) {
		console.error("An error occurred:", error);
	}
}

// Run the import process
importXmlToJson(path.resolve(process.cwd(), "in", "context.xml"));
