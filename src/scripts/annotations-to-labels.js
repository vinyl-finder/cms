#!/usr/bin/env node

import fs from "fs/promises";
import path from "path";
import xml2js from "xml2js";

// Function to parse XML data
async function parseXml(xmlData) {
	return await xml2js.parseStringPromise(xmlData);
}

// Function to write JSON data to file
async function writeJsonToFile(filePath, jsonData) {
	await fs.writeFile(filePath, JSON.stringify(jsonData, null, 2));
}

// Main function to convert XML to JSON files for each label
async function convertXmlToLabelsJson(xmlFilePath) {
	try {
		// Read XML file
		const xmlData = await fs.readFile(xmlFilePath, "utf-8");

		// Parse XML data
		const parsedXml = await parseXml(xmlData);

		// Extract annotations from parsed XML
		const annotations = parsedXml.Annotations.Annotation;

		// Get directory path of current working directory
		const cwd = process.cwd();

		// Create directory for JSON files if it doesn't exist
		const outputDir = path.join(cwd, "content", "labels");
		await fs.mkdir(outputDir, { recursive: true });

		// Extract unique labels from annotations
		const labelsSet = new Set();
		annotations.forEach((annotation) => {
			if (Array.isArray(annotation.Label)) {
				annotation.Label.forEach((label) => {
					labelsSet.add(label.$.name);
				});
			} else {
				labelsSet.add(annotation.Label.$.name);
			}
		});
		const labels = Array.from(labelsSet).filter((label) => {
			return !label.startsWith("_cse_");
		});

		// Write each label to a separate JSON file
		await Promise.all(
			labels.map(async (label) => {
				const jsonFileName = `${label}.json`;
				const jsonFilePath = path.join(outputDir, jsonFileName);

				// Write JSON object to file
				await writeJsonToFile(jsonFilePath, { name: label });

				console.log(`Label JSON file created: ${jsonFileName}`);
			}),
		);

		console.log("Conversion completed.");
	} catch (error) {
		console.error("An error occurred:", error);
	}
}

// Convert XML to JSON files for each label
convertXmlToLabelsJson(path.resolve(process.cwd(), "in/annotations.xml"));
