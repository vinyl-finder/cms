#!/usr/bin/env node

import fs from "fs";
import path from "path";
import { diffLines } from "diff";

function readFile(filePath) {
	return fs.readFileSync(filePath, "utf-8");
}

const __dirname = path.dirname(new URL(import.meta.url).pathname);
const inFilePath = path.join(__dirname, "../../in/annotations.xml");
const outFilePath = path.join(__dirname, "../../out/annotations.xml");

const inXml = readFile(inFilePath);
const outXml = readFile(outFilePath);

const diff = diffLines(inXml, outXml);

diff.forEach((part) => {
	const color = part.added ? "\x1b[32m" : part.removed ? "\x1b[31m" : "\x1b[0m";
	process.stdout.write(color + part.value);
});

console.log("\x1b[0m"); // Reset color
