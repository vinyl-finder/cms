#!/usr/bin/env node

import fs from "fs/promises";
import path from "path";
import xml2js from "xml2js";

// Function to parse XML data
async function parseXml(xmlData) {
	return await xml2js.parseStringPromise(xmlData);
}

// Function to write JSON data to file
async function writeJsonToFile(filePath, jsonData) {
	await fs.writeFile(filePath, JSON.stringify(jsonData, null, 2));
}

// Main function to convert XML to JSON files
async function convertXmlToSitesJson(xmlFilePath) {
	try {
		// Read XML file
		const xmlData = await fs.readFile(xmlFilePath, "utf-8");

		// Parse XML data
		const parsedXml = await parseXml(xmlData);

		// Extract annotations from parsed XML
		const annotations = parsedXml.Annotations.Annotation;

		// Get directory path of current working directory
		const cwd = process.cwd();

		// Create directory for JSON files if it doesn't exist
		const outputDir = path.join(cwd, "content", "sites");
		await fs.mkdir(outputDir, { recursive: true });

		// Extract unique sites from annotations
		const sites = [];
		const seenDomains = new Set();

		annotations.forEach((annotation) => {
			const url = annotation.$.about;
			// Google CSE are not prefixed with the protocol schema
			const domain = new URL("https://" + url).hostname;

			if (seenDomains.has(domain)) {
				console.warn(`Duplicate domain found: ${domain}`);
			} else {
				seenDomains.add(domain);

				const site = {
					id: domain,
					url,
					name: url,
					labels: [],
				};

				let labels;
				if (Array.isArray(annotation.Label)) {
					labels = annotation.Label.map((label) => label.$.name).filter(
						(label) => {
							// remove the `_cse_<id>` part
							return !label.startsWith("_cse_");
						},
					);
				} else {
					labels = [annotation.Label.$.name];
				}

				site.labels.push(...labels);
				sites.push(site);
			}
		});

		// Write each site to a separate JSON file
		await Promise.all(
			sites.map(async (site) => {
				const jsonFileName = `${site.id}.json`;
				const jsonFilePath = path.join(outputDir, jsonFileName);

				// Write JSON object to file
				await writeJsonToFile(jsonFilePath, site);

				console.log(`Site JSON file created: ${jsonFileName}`);
			}),
		);

		console.log("Conversion completed.");
	} catch (error) {
		console.error("An error occurred:", error);
	}
}

// Convert XML to JSON files for each site
convertXmlToSitesJson(path.resolve(process.cwd(), "in/annotations.xml"));
