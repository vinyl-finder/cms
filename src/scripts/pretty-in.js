#!/usr/bin/env node

import { readdir, readFile, writeFile } from "fs/promises";
import xmlFormatter from "xml-formatter";
import path from "path";

async function prettyPrintXmlFiles(folderPath) {
	try {
		const files = await readdir(folderPath);
		await Promise.all(
			files.map(async (file) => {
				if (path.extname(file) === ".xml") {
					const filePath = path.join(process.cwd(), folderPath, file);
					const xml = await readFile(filePath, "utf-8");
					const formattedXml = xmlFormatter(xml, { indentation: "\t" });
					await writeFile(filePath, formattedXml);
					console.log(`Pretty printed XML file saved at ${filePath}`);
				}
			}),
		);
	} catch (error) {
		console.error("Error while pretty printing XML files:", error);
	}
}

prettyPrintXmlFiles("in");
