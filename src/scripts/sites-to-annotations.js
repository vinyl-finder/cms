#!/usr/bin/env node

import { readdir, readFile, writeFile, mkdir } from "fs/promises";
import path from "path";
import xmlFormatter from "xml-formatter";

const sitesFolder = path.join(process.cwd(), "content", "sites");
const outputFolder = path.join(process.cwd(), "out");

const annotations = [];

async function readFiles() {
	try {
		// Ensure output folder exists or is created
		await ensureOutputFolderExists();

		const files = await readdir(sitesFolder);
		await Promise.all(
			files.map(async (file) => {
				if (path.extname(file) === ".json") {
					const filePath = path.join(sitesFolder, file);
					const data = await readFile(filePath, "utf-8");
					let site;
					try {
						site = JSON.parse(data);
					} catch (error) {
						console.error("Error parsing JSON:", filePath);
					}
					try {
						annotations.push(siteToXml(site));
					} catch (e) {
						console.error("Error converting JSON to xml", site);
					}
				}
			}),
		);

		const sortedAnnotations = annotations.sort((a, b) => {
			a.url > b.url;
		});

		if (sortedAnnotations.length > 0) {
			await generateAnnotationsXml(sortedAnnotations);
		} else {
			console.log("No JSON files found in the sites folder.");
		}
	} catch (err) {
		console.error("Error reading sites folder:", err);
	}
}

async function ensureOutputFolderExists() {
	try {
		// Check if output folder exists
		await readdir(outputFolder);
	} catch (err) {
		// If output folder doesn't exist, create it
		if (err.code === "ENOENT") {
			await mkdir(outputFolder, { recursive: true });
			console.log("Output folder created:", outputFolder);
		} else {
			throw err;
		}
	}
}

function siteToXml(site) {
	if (
		!site.labels.indexOf("_exclude_") > -1 &&
		!site.labels.indexOf("_include_") > -1
	) {
		site.labels = [...site.labels, "_include_"];
	}

	const labels = site.labels
		.map((label) => `<Label name="${label}"/>`)
		.join("\n");

	return `
<Annotation about="${site.url}">
	${labels}
</Annotation>
	`;
}

async function generateAnnotationsXml(annotations) {
	try {
		const xml = `
<Annotations>
	${annotations.join("\n")}
</Annotations>`;
		const formattedXml = xmlFormatter(xml);
		const outputPath = path.join(outputFolder, "annotations.xml");
		await writeFile(outputPath, formattedXml);
		console.log("Annotations XML file generated:", outputPath);
	} catch (err) {
		console.error("Error writing annotations XML file:", err);
	}
}

readFiles();
