const app_id =
	"0b8f76cd84d70fedc5d0b49588a693493bf641123d3f7baed42de243c6a27a75";

const PRODUCTION = window.location.hostname === "vinyl-finder.gitlab.io";

export default {
	config: {
		load_config_file: false,
		display_url: "https://vinyl-finder.gitlab.io",

		local_backend: PRODUCTION ? false : true,
		backend: {
			name: "gitlab",
			repo: "vinyl-finder/data",
			branch: "main",
			auth_type: "pkce",
			app_id,
		},

		media_folder: "content/media/uploads",
		public_folder: "content/media/uploads",
		logo_url: "content/media/logo.png",

		collections: [
			{
				name: "sites",
				label: "Sites",
				label_singular: "Site",
				folder: "content/sites",
				format: "json",
				create: true,
				slug: "{{name}}",
				editor: {
					preview: false,
				},
				description:
					"Sites referenced in the search engine. Each site should offer the possibility to order vinyls on the pages that will be indexed by the search engine.",
				sortable_fields: ["title", "slug"],
				fields: [
					{
						label: "Name",
						name: "name",
						widget: "string",
						hint: "The name of the website for this vinyl record shop",
						required: true,
					},
					{
						label: "Url",
						name: "url",
						widget: "string",
						hint: "The url pattern of the website for this vinyl record shop (no http://) Ex: www.example.com/my-catalog/* to index all sub pages.",
						required: true,
					},
					{
						label: "Labels",
						name: "labels",
						widget: "relation",
						collection: "labels",
						search_fields: ["name"],
						value_field: "name",
						multiple: true,
						required: true,
					},
				],
			},
			{
				name: "labels",
				label: "Labels",
				label_singular: "Label",
				folder: "content/labels",
				format: "json",
				create: true,
				slug: "{{name}}",
				editor: {
					preview: false,
				},
				description:
					"Labels are grouping categories associated with the sites, for example geographic zones to help organizing search results.",
				sortable_fields: ["name"],
				fields: [
					{
						label: "Name",
						name: "name",
						widget: "string",
						hint: "The name of the category, label applied as groupping tag on a site",
						required: true,
					},
				],
			},
		],
	},
};
